# Simple Cafe Billing Service
 
Scenario

Cafe X menu consists of the following items:

1) Cola – 50 cents
2) Coffee - $1.00
3) Cheese Sandwich - $2.00
4) Steak Sandwich - $4.50

Customers don’t know how much to tip and staff need tips to survive!
Write some code to generate a bill including a service charge so customers don’t have to work out how much to tip.

1.	Pass in a list of purchased items that produces a total bill
2.	When all purchased items are drinks no service charge is applied
3.	When purchased items include any food apply a service charge of 10% to the total bill (rounded to 2 decimal places)