
from resources import constant

class CafeBllingService():

    def getCustomerOrder(self):

        customerOrder = {}
        print("Welcome to Caffe ! Please order here ")
        cocaColaCount = int(input("Enter number of Coca Cola ( $"+ str(constant.COCACOLA_PRICE) +" ):"))
        customerOrder["cocaCola"] = cocaColaCount

        coffeeCount = int(input("Enter number of Coffee ( $"+ str(constant.COFFEE_PRICE) +" ):"))
        customerOrder["coffee"] = coffeeCount

        cheseSandCount = int(input("Enter number of Cheese Sandwitch ( $"+ str(constant.CHEESE_SANDWITCH_PRICE) +" ):"))
        customerOrder["cheeseSandwitch"] = cheseSandCount

        coffeeCount = int(input("Enter number of Steak Sandwitch ( $"+ str(constant.STEAK_SANDWITCH_PRICE) +" ):"))
        customerOrder["steakSandwitch"] = coffeeCount

        return customerOrder


    def calculateTotalPrice(self, customerOrder):

        totalPrice = 0
        servicetax = False

        cocacola = customerOrder["cocaCola"]  * constant.COCACOLA_PRICE
        coffe = customerOrder["coffee"] * constant.COFFEE_PRICE
        cheesSandwitch = customerOrder["cheeseSandwitch"] * constant.CHEESE_SANDWITCH_PRICE
        steakSandwitch = customerOrder["steakSandwitch"] * constant.STEAK_SANDWITCH_PRICE

        if customerOrder["cocaCola"] != 0:
            totalPrice += customerOrder["cocaCola"] * constant.COCACOLA_PRICE

        if customerOrder["coffee"] != 0:
            totalPrice += customerOrder["coffee"] * constant.COFFEE_PRICE

        if customerOrder["cheeseSandwitch"] != 0 :
            totalPrice += customerOrder["cheeseSandwitch"] * constant.CHEESE_SANDWITCH_PRICE
            servicetax = True

        if customerOrder["cheeseSandwitch"] != 0:
            totalPrice += customerOrder["cheeseSandwitch"] * constant.CHEESE_SANDWITCH_PRICE
            servicetax = True

        print("Subtotal : {}".format(totalPrice))

        # Add 10% service charge
        if servicetax:
            totalPrice  = totalPrice + ( totalPrice / 100 ) * 10
            print("SC (10%) : {}".format(( totalPrice / 100 ) * 10))
        else:
            print("SC (10%) : NA")

        return totalPrice

